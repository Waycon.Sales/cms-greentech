## CMS - GreenTech
O GreenTech é um projeto de gerenciamento de conteúdo na web, no qual o usuário não precisará saber tecnologias web para postar seu conteúdo no site. Através do cms Green Tech o usuário poderá administrar o site GeekSide, que é voltado para o público geek, postando vídeos, fotos e escrevendo em uma página, algo semelhante a um blog sem a necessidade de escrever linhas de código. As tecnologias utilizadas foram: PHP, JS, MYSQL, HTML, CSS, framework Bootstrap... O usuário também pode personalizar o site geek side como quiser, ao escolher imagens, textos e vídeos que o representa.

### Produzido em 2019.2, Autor: Antonio Waycon, desenvolvedor web. Créditos: Antonio Waycon.
