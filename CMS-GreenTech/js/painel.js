/*--------------Javascript Puro---------------*/
function mostrar(id){
	var menu = document.getElementById(id);
	if(menu.classList.contains("parece-hide")){
		menu.classList.remove("parece-hide");		
	}else{
		menu.classList.add("parece-hide");
	}
}

function preview(event, id){
	var ler = new FileReader();
	var localimg = document.getElementById(id);
	ler.onload = function(){
		if(ler.readyState == 2){
			localimg.src = ler.result;
		}
	}
	ler.readAsDataURL(event.target.files[0]);
}
	
function display(id){
	var conteudo = document.getElementById(id);
	if(conteudo.style.display=="block"){
		conteudo.style.display="none";
	}else{
		conteudo.style.display="block"
	}
}


/*--------------JQuery:checagem de campos, Ajax---------------*/

/*--------------Inserir post de game no site (VALIDAÇÃO DE CAMPOS )---------------*/
$('#formpost').submit(function(){
		var titulo = $('#titulo').val();
		var lead = $('#lead').val();
		var texto = $('#textoInserir').val();
		var form
		if(titulo=='' || lead=='' || texto==''){
			swal({
  				title: "Preencha todos os campos!",
  				text: "todos os campos são obrigatórios",
  				icon: "warning"
			});
			return false;
		}
	});

/*--------------Requisição de Cadastro---------------*/
$('#cadastrar').click(function(e){
		e.preventDefault();
		var user = $('#user');
		var email = $('#email');
		var senha= $('#senha');
		var confsenha = $('#confsenha');

		if(user.val()=='' || senha.val()=='' || email.val()=='' || confsenha.val()==''){
			swal({
  				title: "Preencha todos os campos!",
  				text: "todos os campos são obrigatórios",
  				icon: "warning"
			});

			return false;
		}
		$.ajax({
			url:'../backend/inseriradm.php',
			method:'POST',
			datatype:"html",
			data:{
				user: user.val(),
				email: email.val(),
				senha: senha.val(),
				confsenha: confsenha.val()
			}
		}).done(function(data){
			if(data=="Um novo administrador foi adicionado com sucesso!"){
				user.val('');
				email.val('');
				senha.val('');
				confsenha.val('');
				swal({
  					title: data,
  					text: "Agora outro usuario do GreenTech já está disponivel",
  					icon: "success"
				});
			}else{
				swal({
  					title: "Por favor, tente novamente.",
  					text: data,
  					icon: "warning"
				});
				senha.val('');
				confsenha.val('');
			}
		});

	});

/*--------------Modificação de senha---------------*/

$('#mudsenha').click(function(e){
		e.preventDefault();
		var id = $('#id');
		var senhaatual= $('#atualsenha');
		var novasenha = $('#novasenha');
		var confsenha = $('#confsenha');

		if(senhaatual.val()=='' || novasenha.val()=='' || confsenha.val()==''){
			swal({
  				title: "Preencha todos os campos!",
  				text: "todos os campos são obrigatórios",
  				icon: "warning"
			});

			return false;
		}
		$.ajax({
			url:'../backend/mudarsenha-adm.php',
			method:'POST',
			datatype:"html",
			data:{
				id: id.val(),
				atualsenha: senhaatual.val(),
				novasenha: novasenha.val(),
				confsenha: confsenha.val()
			}
		}).done(function(data){
			if(data=="senha modificada"){
				window.location.href="login.php";
			}else{
				swal({
  					title: "Por favor, tente novamente.",
  					text: data,
  					icon: "warning"
				});
				senhaatual.val('');
				novasenha.val('');
				confsenha.val('');
			}
		});

	});

/*--------------Modificação de edição de textos do site---------------*/
$('#editsitetxt').click(function(e){
		e.preventDefault();
		var id = $('#id').val();
		var conteudo = $('#conteudo').val();
		$.ajax({
			url:'../backend/editarSitetxt.php',
			method:'POST',
			datatype:"html",
			data:{
				id: id,
				conteudo: conteudo
			}
		}).done(function(data){
			if(data=="Texto modificado com sucesso!"){
				swal({
  					title: data,
  					icon: "success"
				});
				
			}else{
				swal({
  					title: "Por favor, tente novamente.",
  					text: data,
  					icon: "warning"
				});
				
			}
		});

	});

