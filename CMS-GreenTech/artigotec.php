<?php
require_once("backend/sitegeral-controle.php");
require_once("backend/post-controle.php");
$sitecontrol = new SitegeralControle();
$postcontrol = new PostControle();
$id=4;
$facelink = $sitecontrol->selecionarUm($id);
$id=5;
$instalink = $sitecontrol->selecionarUm($id);
$id=6;
$sobre = $sitecontrol->selecionarUm($id);
$id = $_GET['id'];
$tabela="posttec";
$posttec=$postcontrol->selecionarUm($id,$tabela);
echo '
<html lang="pt-br">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/artigo.css">
     <link rel="shortcut icon" href="imagens/logosite.svg">
    <title>post-tecnologia</title>
</head>
<body>
	
<!----------------------------------------SLIDE---------------------------------------------->
			  <header id="topo">
			  	<div id="slideHome" class="carousel slide" data-ride="carousel">
			   	 <div class="carousel-inner">
			      	<div class="carousel-item active">
			        		<img id="img-1" class="d-block w-100 img img-fluid" src="backend/carregasite-midia.php?id=1" alt="Não foi possivel carregar o primeiro slide"/>
			      	</div>
			  	    <div class="carousel-item">
			  	     	<img id="img-2" class="d-block w-100 img" src="backend/carregasite-midia.php?id=2" alt="Não foi possivel carregar o segundo slide"/>
			  	    </div>
			  	    <div class="carousel-item">
			        		<img id="img-3" class="d-block w-100 img" src="backend/carregasite-midia.php?id=3" alt="Não foi possivel carregar o terceiro slide"/>
			      	</div>
			      	<div class="carousel-item">
			        		<img id="img-4" class="d-block w-100 img" src="backend/carregasite-midia.php?id=4" alt="Não foi possivel carregar o quarto slide"/>
			      	</div>
			       	<div class="carousel-item">
			        		<img id="img-5" class="d-block w-100 img" src="backend/carregasite-midia.php?id=5" alt="Não foi possivel carregar o quinto slide"/>
			      	</div>
			        </div>
			    	</div>
			  </header>

<!---------------------------------------------MENU------------------------------------------->

				<nav  class="navbar navbar-expand-lg navbar-dark naveg">
					<div class="logoEnome">
						<img class="logo mr-2" src="backend/carregasite-midia.php?id=15" alt="ainda sem logo">
			   			<span class="navbar-brand namesite"><strong>Geek Side</strong></span>
			   		</div>
			   		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu">
			   			<span class="navbar-toggler-icon"></span> 
			   		</button>
					<div id="menu" class="collapse navbar-collapse">
						<ul class="navbar-nav ml-auto">
							<li class="nave-item">
								<a class="link  " href="index.php">Home</a>						
							</li>
							<li class="nave-item">
								<a class=" link" href="zona-game.php">Zona Gamer</a>						
							</li>
							<li class="nave-item">
								<a class=" link" href="filmes.php">Filmes Geek</a>						
							</li>
							<li class="nave-item">
								<a class="link" href="tecnologia.php">Tecnologia</a>						
							</li>
			        <li class="nave-item">
			            <a class="link" href="#footer">Sobre Nós</a>           
			        </li>
							<li class="nave-item">
								<a href="adm/login.php"><img class="login" src="imagens/iconuser.svg"></a>
							</li>
						</ul>		
					</div>
				</nav>


<!------------------------------------conteudo---------------------------------->
<main class="conteudo mt-5 mb-5">
		<div class="container">
		<div class="artigo">
		<div class="container ">
		<div class="centro">
			<h2 class="text-justify mt-5 titulo tit">'; echo $posttec->titulo; echo'</h2>
		</div>
		<div class=" centro">
			<h4 class="text-justify mt-3 lead">'; echo $posttec->lead; echo'</h5>
		</div>				
		<div class="centro">
				<img src="backend/carregatec-midia.php?id='; echo $posttec->id; echo'" alt="erro" class="imagem img-fluid"/>
		</div> 
		<div class="just">
			<h6 class="text-justify txt-post mt-3 mb-5">
				'; echo $posttec->texto; echo'
			</h6>
		</div>
		</div>
		</div>					
		</div>
	</div>
	</main>

<!------------------------------------FOOTER----------------------------------->

			  <div class="container-fluid">
			    <div id="footer" class="footer">
			      <div class="footer-content">
			        <div class="row mx-lg-n5">
			        <div class="col-12 col-md voltar-topo">
			            <a href="#topo"><img class="img-fluid" src="imagens/topo.svg" alt="voltar ao topo"/></a>
			          </div>
			          <div class="col-6 col-md">
			            <h2 class=" txt-foot text-center">Sobre Nós</h2>
			            <div id="sobre" class="sobre">
			              '; echo $sobre->conteudo; echo'
			            </div>
			          </div>
			          <div class="col-6 col-md">
			            <h2 class="txt-foot text-center ">Siga-nos</h2>
			            <div class="text-center redes">
			              <a target="_blank" href="'; echo $facelink->conteudo; echo'"><span class="face">facebook</span></a>
			              <a target="_blank" href="'; echo $instalink->conteudo; echo'"><span class="insta">instagram</span></a>
			            </div>
			          </div>
			        </div>    
			      </div>
			      <div class="footer-bottom">
			        &copy; greentech | @waycon.sales
			      </div>
			    </div>
			  </div>
</div>
</div>

</body>
	<script src="js/jquery.min.js"></script>
  	<script src="js/bootstrap.min.js"></script>
  	<script src="js/pace.min.js"></script>
</html>
	


	';



?>