<?php
require_once("../backend/adm-controle.php");
session_start();
if($_SESSION['user']){
$adm = new Adm();
$admcontrol = new AdmControle();
$cond=$_SESSION['user'];
$campo='user';
$dados = $admcontrol->selecionarCond($campo,$cond);
$adm->setId($dados->id);
$adm->setUser($dados->user);
$adm->setEmail($dados->email);


echo '       
<html lang="pt-br">
<head>
	<title>Conta</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/painel.css">
	<link rel="shortcut icon" href="../imagens/logocms.svg">
</head>
<body>
	<div class="container-fluid header row mx-lg-n5">
		<div class="container-fluid logoEnome">
			<img src="../imagens/logocms.svg" alt="erro">
			<h1>GreenTech</h1>
		</div>	
		<div class="nav-site">
			<div class="usernome">';echo $_SESSION['user']; echo'</div>
			<a href="#"><img title="Conta" class="contaicon" src="../imagens/conta.svg" alt="erro"></a>
			<a href="../index.php"><img title="Voltar a home" class="homeicon" src="../imagens/home.svg" alt="erro"></a>
			<a href="../backend/sair.php"><img title="Logout" class="sair" src="../imagens/exit.svg" alt="erro"></a>	
			<input id="menuicon" type="checkbox" name="menu" class="inp">
			<label class="bot-home" onclick="mostrar(\'menu\')" for="menuicon" ><img class="iconmenu" src="../imagens/menucms.svg" alt="erro-icon"/></label>
		</div>
	</div>

	<div  class="container-fluid area-interacao row mx-lg-n5 ">
		<div  class="container-fluid row mx-lg-n5 ">
			<div class="conteudo container-fluid col-11 col-md">
				<div class="container-fluid formulario-add ">
					<div class="text-center mb-3">
						<h3 class="txt-saudacao">Seus dados</h3>
					</div>
					<div class="centro">
						<img class=" img-fluid avatar" alt="erro ao carregar imagem" src="../imagens/undraw_male_avatar_323b.svg">
					</div>
					<div class="container-fluid dados mt-3">
						<label class="txt-label txt-left"><span class="lbl-userdata">User: </span>';echo $adm->getUser(); echo' </label>
					</div>
					<div class="container-fluid dados mt-3">
						<label class="txt-label"><span class="lbl-userdata">Email: </span>';echo $adm->getEmail(); echo'</label>
					</div>
					<button id="btn-mod" class="mt-2 btn-mod" onclick="display(\'formod\')">Alterar user</button>
					<button id="btn-mod" class="mt-2 btn-mod" onclick="display(\'formod2\')">Alterar senha</button>
					<div class="mt-2">
						<a href="cadastro-adm.php" class=" add-adm">Add admin</a>
					</div>
					<form  action="';echo "../backend/mudaruser-adm.php?id={$dados->id}"; echo'" class="mt-2" method="POST" id="formod">
							<div class="form-group">
					    		<label for="user" class="txt-label">Usuário</label>
					    		<input type="text" class="form-control" name="user" id="user"  value="';echo "{$adm->getUser()}"; echo'" placeholder="Nome de usuário">
					  		</div>
							<div class="form-group">
					    		<label for="email" class="txt-label">Email</label>
					    		<input type="email" name="email" value="';echo "{$adm->getEmail()}"; echo'" class="form-control" id="email"  placeholder="Seu email">
					  		</div>
							<div class="centro mb-5">
			  					<button type="submit" class="btn mb-2 text-center">Alterar</button>
			  				</div>	
			  				</form>

			  				<form  class="mt-2"  id="formod2">
							<div class="form-group">
					    		<label for="atualsenha" class="txt-label">Senha Atual</label>
					    		<input type="password" class="form-control" id="atualsenha"  name="atualsenha" placeholder="Senha atual">
					  		</div>
					  		<div class="form-group mt-2">
					    		<label for="senha" class="txt-label">Nova Senha</label>
					    		<input type="password" class="form-control" name="novasenha" id="novasenha" placeholder="Nova senha">
					  		</div>
					  		<div class="form-group mt-2">
					    		<label for="confsenha" class="txt-label">Confirmação de senha</label>
					    		<input type="password" class="form-control" name="confsenha" id="confsenha" placeholder="Confirme sua senha">
					  		</div>
					  		<input type="hidden" name="id" id="id" value="';echo $dados->id; echo'"/>
							<div class="centro mb-5">
			  					<button type="submit" id="mudsenha" class="btn mb-2 text-center">Alterar</button>
			  				</div>	
			  				</form>

				</div>
			</div>
			<nav id="menu" class="menu col-4 col-md parece-hide">
				<div class="item1">
					<div class="item">
						<input type="checkbox" id="home" name="check">
						<label for="home">Site</label>
						<ul>
							<li><a href="home-adm.php">SiteMidia</a></li>
							<li><a href="sitegeral-adm.php">Sitetextos</a></li>
						</ul>
					</div>
					<div class="item">
						<input type="checkbox" id="zg" name="check">
						<label for="zg">Zona Gamer</label>
						<ul>
							<li><a href="game-adm.php">Add Post</a></li>
							<li><a href="gerenciagame-adm.php">Gerenciar</a></li>
						</ul>
					</div>
					<div class="item">
						<input type="checkbox" id="filmes" name="check">
						<label for="filmes">Filmes</label>
						<ul>
							<li><a href="filmes-adm.php">Add Post</a></li>
							<li><a href="gerenciafilmes-adm.php">Gerenciar</a></li>
						</ul>
					</div>
					<div class="item">
						<input type="checkbox" id="tec" name="check">
						<label for="tec">Tecnologia</label>
						<ul>
							<li><a href="tec-adm.php">Add Post</a></li>
							<li><a href="gerenciatec-adm.php">Gerenciar</a></li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</div>	
<!------------FOOTER----------------->
  <div class="container-fluid mt-3">
    <div id="footer" class="footer">
      <div class="footer-content">
        <div class="row mx-lg-n5">
          <div class="col-6 col-md">
            <h2 class="txt text-center">Sobre Nós</h2>
            <div id="sobre" class="sobre">
             GreenTech é um cms criado para facilitar o gerenciamento de dados do seu site de forma eficiente e fácil, obrigado pela preferência. By: Waycon Sales
            </div>
          </div>
          <div class="col-6 col-md">
            <h2 class="txt text-center">Siga-nos</h2>
            <div class="text-center redes">
              <a target="_blank" href="https://www.facebook.com/profile.php?id=100010084095833"><span class="face">facebook</span></a>
              <a target="_blank" href="https://www.instagram.com/waycon.salles/"><span class="insta">instagram</span></a>
            </div>
          </div>
        </div>    
      </div>
      <div class="footer-bottom">
        &copy; GreenTech | @waycon.sales
      </div>
    </div>
  </div>
</body>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/sweetalert.min.js"></script>
<script type="text/javascript" src="../js/painel.js"></script>

</html>
';

}else{
	header("Location: login.php");
}

//action="';echo "controle/mudarsenha-adm.php?id={$dados->id}"; echo'"

?>