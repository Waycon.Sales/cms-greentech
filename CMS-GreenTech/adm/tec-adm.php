<?php
session_start();
if($_SESSION['user']){
	echo '
		<html lang="pt-br">
			<head>
				<title>Tecnologia-adm</title>
				<meta charset="utf-8">
				<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
				<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
				<link rel="stylesheet" type="text/css" href="../css/painel.css">
				<link rel="shortcut icon" href="../imagens/logocms.svg">
			</head>
			<body>
				<div class="container-fluid header row mx-lg-n5">
					<div class="container-fluid logoEnome">
						<img src="../imagens/logocms.svg" alt="erro">
						<h1>GreenTech</h1>
					</div>	
					<div class="nav-site">
						<div class="usernome">';echo $_SESSION['user']; echo'r</div>
						<a href="conta-adm.php"><img title="Conta" class="contaicon" src="../imagens/conta.svg" alt="erro"></a>
						<a href="../index.php"><img title="Voltar a home" class="homeicon" src="../imagens/home.svg" alt="erro"></a>
						<a href="../backend/sair.php"><img title="Logout" class="sair" src="../imagens/exit.svg" alt="erro"></a>	
						<input id="menuicon" type="checkbox" name="menu" class="inp">
						<label class="bot-home" onclick="mostrar(\'menu\')" for="menuicon" ><img class="iconmenu" src="../imagens/menucms.svg" alt="erro-icon"/></label>
					</div>
				</div>

				<div  class="container-fluid area-interacao row mx-lg-n5 ">
					<div  class="container-fluid row mx-lg-n5 ">
						<div class="conteudo container-fluid col-11 col-md">
							<div class="container-fluid formulario-add ">
								<div class="text-center mb-1">
									<h3 class="txt-saudacao">Adicione um post sobre Tecnologia</h3>
								</div>
								<form id="formpost" action="../backend/inserirPOSTTEC.php" method="POST" enctype="multipart/form-data">
									<div class="form-group">
										<label class="txt-label" for="titulo">Título da postagem</label>
										<input class="form-control" type="text" name="titulo" id="titulo" placeholder="Digite o título"/>
									</div>
									<div class="form-group">
										<label class="txt-label" for="lead">Lead da postagem</label>
										<input class="form-control" type="text" name="lead" id="lead" placeholder="Digite o Lead"/>
									</div>
				 					<div class="form-group">
				    					<label class="add-img" for="file">
				    						<i><img src="../imagens/addfoto.svg" alt="erro" /></i>	
				    						Imagem ou video
				    					</label>
				    					<input onchange="preview(event,\'imagem\')" type="file" class="form-control-file" name="upload" id="file"/>
				    					<img class="img-fluid mt-3 preview" id="imagem" src="" />
				 					</div>
				 					<div class="form-group">
				    					<label class="txt-label" for="textoInserir">Adicione o texto da postagem</label>
				    					<textarea class="form-control" id="textoInserir" name="txtInserir" /></textarea>		
				 	 				</div>
				 	 				<div class="botao">
				 	 					<button type="submit" class="btn ">Enviar</button>
									</div>
								</form>
							</div>
						</div>
						<nav id="menu" class="menu col-4 col-md parece-hide">
							<div class="item1">
								<div class="item">
									<input type="checkbox" id="site" name="check">
									<label for="site">Site</label>
									<ul>
										<li><a href="home-adm.php">SiteMidia</a></li>
										<li><a href="sitegeral-adm.php">Sitetextos</a></li>
									</ul>
								</div>
								<div class="item">
									<input type="checkbox" id="zg" name="check">
									<label for="zg">Zona Gamer</label>
									<ul>
										<li><a  href="game-adm.php">Add Post</a></li>
										<li><a href="gerenciagame-adm.php">Gerenciar</a></li>
									</ul>
								</div>
								<div class="item">
									<input type="checkbox" id="filmes" name="check">
									<label for="filmes">Filmes</label>
									<ul>
										<li><a href="filmes-adm.php">Add Post</a></li>
										<li><a href="gerenciafilmes-adm.php">Gerenciar</a></li>
									</ul>
								</div>
								<div class="item">
									<input type="checkbox" id="tec" name="check">
									<label class="des-l" for="tec">Tecnologia</label>
									<ul>
										<li><a class="des-a" href="#">Add Post</a></li>
										<li><a href="gerenciatec-adm.php">Gerenciar</a></li>
									</ul>
								</div>
							</div>
						</nav>
					</div>
				</div>	
			<!------------FOOTER----------------->
			  <div class="container-fluid mt-3">
			    <div id="footer" class="footer">
			      <div class="footer-content">
			        <div class="row mx-lg-n5">
			          <div class="col-6 col-md">
			            <h2 class="txt text-center">Sobre Nós</h2>
			            <div id="sobre" class="sobre">
			             GreenTech é um cms criado para facilitar o gerenciamento de dados do seu site de forma eficiente e fácil, obrigado pela preferência. By: Waycon Sales
			            </div>
			          </div>
			          <div class="col-6 col-md">
			            <h2 class="txt text-center">Siga-nos</h2>
			            <div class="text-center redes">
			              <a target="_blank" href="https://www.facebook.com/profile.php?id=100010084095833"><span class="face">facebook</span></a>
			              <a target="_blank" href="https://www.instagram.com/waycon.salles/"><span class="insta">instagram</span></a>
			            </div>
			          </div>
			        </div>    
			      </div>
			      <div class="footer-bottom">
			        &copy; GreenTech | @waycon.sales
			      </div>
			    </div>
			  </div>
			</body>
			<script type="text/javascript" src="../js/jquery.min.js"></script>
			<script type="text/javascript" src="../js/bootstrap.min.js"></script>
			<script type="text/javascript" src="../js/sweetalert.min.js"></script>
			<script type="text/javascript" src="../js/painel.js"></script>
			</html>

	';
}else{
	header("Location: login.php");
}
?>
