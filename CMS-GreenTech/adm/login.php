<?php
session_start();
if(isset($_SESSION['user'])){
	header("Location: home-adm.php");
}else{
	echo '
		<html>
			<head>
				<title>Login Oficial</title>
				<meta charset="utf-8">
				<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
				<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
				<link rel="stylesheet" type="text/css" href="../css/login2.css">
				<link rel="shortcut icon" href="../imagens/logocms.svg">
			</head>
			<body>
				<div class="container-fluid cabecalho">	
					<div class=" name-site">		
						<h1>GreenTech</h1>
					</div>
				</div>
				<div class="container-fluid elementos">
					<div class="row mx-lg-n5">
						<div class="col py-6 px-lg-5 coluna1">
							<div class="formulario">
								<form  onSubmit="return validarCampos()" id="form">
									<div class="perfil ">
									<img class=" img-fluid avatar" alt="erro ao carregar imagem" src="../imagens/undraw_male_avatar_323b.svg">
									</div>
									
									<div class="text-center mb-1">
									<h3 class="txt-saudacao">Seja Bem Vindo</h3>
									<span class="medium log-cad">Logue-se</span>
									</div>
									<div class="form-group">
						    			<label for="user" class="font">Usuário</label>
						    			<input type="text" class="form-control" name="user" id="user"  placeholder="Seu nome de usuário"/>
						  			</div>
						  			<div class="form-group mt-2">
						    			<label for="senha" class="font">Senha</label>
						    			<input type="password" class="form-control" name="senha" id="senha" placeholder="Senha"/>
						    			<div class="medium "> <a href="../index.php">Voltar à home</a></div>

						  			</div>
				  					<button type="submit" id="btnenvia" class="btn mb-2 text-center">Entrar</button>
				  				</form>
							</div>
							
						</div>
						<div class="col py-6 px-lg-5 img-dvc">
							<div class="login-svg">
								<img src="../imagens/login.svg">
							</div>
						</div>
					</div>
				</div>

			</body>
			<script type="text/javascript" src="../js/jquery.min.js"></script>
			<script type="text/javascript" src="../js/bootstrap.min.js"></script>
			<script type="text/javascript" src="../js/sweetalert.min.js"></script>
			<script type="text/javascript" src="../js/login.js"></script>
			</html>
';

}
?>
