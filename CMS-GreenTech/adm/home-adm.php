<?php
session_start();

if($_SESSION['user']){
require_once("../backend/midiasite-controle.php");
$midiasitecontrol= new MidiasiteControle();
$dados=$midiasitecontrol->selecionarTodos();
echo '
<html lang="pt-br">
<head>
	<title>Site-Midia</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/gerencia-adm.css">
	<link rel="shortcut icon" href="../imagens/logocms.svg">
</head>
<body>
	<div class="container-fluid header row mx-lg-n5">
		<div class="container-fluid logoEnome">
			<img src="../imagens/logocms.svg" alt="erro">
			<h1>GreenTech</h1>
		</div>	
		<div class="nav-site">
			<div class="usernome">';echo $_SESSION['user']; echo'</div>
			<a href="conta-adm.php"><img title="Conta" class="contaicon" src="../imagens/conta.svg" alt="erro"></a>
			<a href="../index.php"><img title="Voltar a home" class="homeicon" src="../imagens/home.svg" alt="erro"></a>
			<a href="../backend/sair.php"><img title="Logout" class="sair" src="../imagens/exit.svg" alt="erro"></a>
			<input id="menuicon" type="checkbox" name="menu" class="inp">
			<label class="bot-home" onclick="mostrar(\'menu\',\'conteudo\')" for="menuicon" ><img class="iconmenu" src="../imagens/menucms.svg" alt="erro-icon"/></label>	
		</div>
	</div>

	<div  class="container-fluid area-interacao row mx-lg-n5 ">
		<div  class="container-fluid row mx-lg-n5 ">
			<div id="conteudo" class="conteudo container col-12 col-md">
				<div class="container-fluid tabela-dados">
					<div class="text-center mb-1">
						<h3 class="txt-saudacao">Gerencia-Midias-Site</h3>
					</div>
					<div class="container-fluid">
					<div class="table-responsive">
						<caption>Imagens-Slide</caption>
							<table class="table">
		  						<thead>
								    <tr>
								      <th scope="col">id</th>
								      <th scope="col">Referece</th>
								      <th scope="col">Mídia</th>
								      <th scope="col">Editar</th>
								    </tr>
		  						</thead>
		  						<tbody>
								    ';
									foreach($dados as $campos){
            							echo "<tr>";
            								echo "<th scope='row'>{$campos->getId()}</th>";
            								echo "<td>{$campos->getReferente()}</td>";
            								echo "<td>{$campos->getNome()}</td>";
            								echo "<td><a class='bot-edit' href='modificarhome-adm.php?id={$campos->getId()}'>Editar</a></td>";
            							echo "</tr>";
        							}
								echo '
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<nav id="menu" class="menu col-4 col-md parece-hide">
				<div class="item1">
					<div class="item">
						<input type="checkbox" id="home" name="check">
						<label class="des-l" for="home">Site</label>
						<ul>
							<li><a class="des-a" href="home-adm.php">SiteMidia</a></li>
							<li><a href="sitegeral-adm.php">Sitetextos</a></li>
						</ul>
					</div>
					<div class="item">
						<input type="checkbox" id="zg" name="check">
						<label for="zg">Zona Gamer</label>
						<ul>
							<li><a href="game-adm.php">Add Post</a></li>
							<li><a href="gerenciagame-adm.php">Gerenciar</a></li>
						</ul>
					</div>
					<div class="item">
						<input type="checkbox" id="filmes" name="check">
						<label for="filmes">Filmes</label>
						<ul>
							<li><a href="filmes-adm.php">Add Post</a></li>
							<li><a href="gerenciafilmes-adm.php">Gerenciar</a></li>
						</ul>
					</div>
					<div class="item">
						<input type="checkbox" id="tec" name="check">
						<label for="tec">Tecnologia</label>
						<ul>
							<li><a href="tec-adm.php">Add Post</a></li>
							<li><a href="gerenciatec-adm.php">Gerenciar</a></li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</div>	
<!------------FOOTER----------------->
  <div class="container-fluid mt-3">
    <div id="footer" class="footer">
      <div class="footer-content">
        <div class="row mx-lg-n5">
          <div class="col-6 col-md">
            <h2 class="txt text-center">Sobre Nós</h2>
            <div id="sobre" class="sobre">
             GreenTech é um cms criado para facilitar o gerenciamento de dados do seu site de forma eficiente e fácil, obrigado pela preferência. By: Waycon Sales
            </div>
          </div>
          <div class="col-6 col-md">
            <h2 class="txt text-center">Siga-nos</h2>
            <div class="text-center redes">
              <a target="_blank" href="https://www.facebook.com/profile.php?id=100010084095833"><span class="face">facebook</span></a>
              <a target="_blank" href="https://www.instagram.com/waycon.salles/"><span class="insta">instagram</span></a>
            </div>
          </div>
        </div>    
      </div>
      <div class="footer-bottom">
        &copy; GreenTech | @waycon.sales
      </div>
    </div>
  </div>
</body>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/gerencia-adm.js"></script>
</html>
';
}else{
	header("Location: login.php");
}
?>
