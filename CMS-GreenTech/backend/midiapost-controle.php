<?php  
	require_once("conexao.php");  
	require_once("midiapost-modelo.php");	
	class MidiapostControle{
		function removerMidia($idpost,$tabelaMidia){
	            try{
	                $conexao = new Conexao();
	                $cmd = $conexao->getConexao()->prepare("DELETE FROM $tabelaMidia WHERE post=:id");
	                $cmd->bindParam("id", $idpost);
	                if($cmd->execute()){
	                	$conexao->fecharConexao();
	                    return true;
	                }else{
	                	$conexao->fecharConexao();
	                    return false;
	                }
	            }catch(PDOException $e){
	                echo "Erro de PDO: {$e->getMessage()}";
	                return false;
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	                return false;
	            }
	        }
	        function selecionarTodos($tabelaMidia){
	            try{
	                $conexao = new Conexao();
	                $cmd = $conexao->getConexao()->prepare("SELECT * FROM $tabelaMidia;");
	                $cmd->execute();
	                $resultado = $cmd->fetchAll(PDO::FETCH_CLASS, "Midiapost");
	                return $resultado;
	                $conexao->fecharConexao();
	            }catch(PDOException $e){
	                echo "Erro no banco: {$e->getMessage()}";
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	            }
        	}

        	function inserirMidia($midiapost, $tabelaMidia){
	            try{
	                $conexao = new Conexao();
	                $nome = $midiapost->getNome();
            		$tipo = $midiapost->getTipo();
    				$conteudo = $midiapost->getConteudo();
    				$post = $midiapost->getPost();
	                $cmd = $conexao->getConexao()->prepare("INSERT INTO $tabelaMidia(nome,tipo,conteudo,post) VALUES(:n,:t,:c,:p);");
                	$cmd->bindParam("n", $nome);
                	$cmd->bindParam("t", $tipo);
                	$cmd->bindParam("c", $conteudo);
                	$cmd->bindParam("p", $post);  
	                if($cmd->execute()){
	                    $conexao->fecharConexao();
	                    return true;
	                }else{
	                    $conexao->fecharConexao();
	                    return false;
	                }
	            }catch(PDOException $e){
	                echo "Erro do banco: {$e->getMessage()}";
	                return false;
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	                return false;
	            }
        	}

			function selecionarUm($idpost, $tabelaMidia){
	            try{
	                $conexao = new Conexao();	
	                $cmd = $conexao->getConexao()->prepare("SELECT * FROM $tabelaMidia WHERE post=:idp;");
	                $cmd->bindParam("idp", $idpost);
	                $cmd->execute();
	                $resultado = $cmd->fetch(PDO::FETCH_OBJ);
	                return $resultado;
	                $conexao->fecharConexao();
	            }catch(PDOException $e){
	                echo "Erro no banco: {$e->getMessage()}";
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	            }
        	}

        	function selecionarCond($campo,$condicao, $tabelaMidia){
	            try{
	                $conexao = new Conexao();	
	                $cmd = $conexao->getConexao()->prepare("SELECT * FROM $tabela WHERE $campo=:cond;");
	                $cmd->bindParam("cond", $condicao);
	                $cmd->execute();
	                $resultado = $cmd->fetch(PDO::FETCH_OBJ);
	                return $resultado;
	                $conexao->fecharConexao();
	            }catch(PDOException $e){
	                echo "Erro no banco: {$e->getMessage()}";
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	            }
        	}             

        	function atualizarMidia($idpost, $midiapost, $tabelaMidia){
            	try{
	                $conexao = new Conexao();
	            	$nome = $midiapost->getNome();
	            	$tipo = $midiapost->getTipo();
	    			$conteudo = $midiapost->getConteudo();
	                $cmd = $conexao->getConexao()->prepare("UPDATE $tabelaMidia SET nome = :nome, tipo = :tipo, conteudo = :cont WHERE post=:idp;");
	                $cmd->bindParam("idp", $idpost);
	                $cmd->bindParam("nome", $nome);
	                $cmd->bindParam("tipo", $tipo);
	                $cmd->bindParam("cont", $conteudo);              
					if($cmd->execute()){
						$conexao->fecharConexao();
						return true;
					}else{
						$conexao->fecharConexao();
						return false;
					}
            	}catch(PDOException $e){
                	echo "Erro no banco: {$e->getMessage()}";
            	}catch(Exception $e){
                	echo "Erro geral: {$e->getMessage()}";
            	}
        	}
		
	}

?>