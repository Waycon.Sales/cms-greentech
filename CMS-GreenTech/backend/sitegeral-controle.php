<?php  
	require_once("conexao.php");
	require_once("sitegeral-modelo.php");
	class SitegeralControle{
		function selecionarTodos(){
	        try{
	            $conexao = new Conexao();
	            $cmd = $conexao->getConexao()->prepare("SELECT * FROM site;");
	            $cmd->execute();
	            $resultado = $cmd->fetchAll(PDO::FETCH_CLASS, "Sitegeral");
	            return $resultado;
	            $conexao->fecharConexao();
	        }catch(PDOException $e){
	                echo "Erro no banco: {$e->getMessage()}";
	        }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	        }
        }

        function selecionarUm($id){
	        try{
	           	$conexao = new Conexao();	
	           	$cmd = $conexao->getConexao()->prepare("SELECT * FROM site WHERE id=:id;");
	            $cmd->bindParam("id", $id);
	            $cmd->execute();
	            $resultado = $cmd->fetch(PDO::FETCH_OBJ);
	         	return $resultado;
	            $conexao->fecharConexao();
	        }catch(PDOException $e){
	            echo "Erro no banco: {$e->getMessage()}";
	        }catch(Exception $e){
	            echo "Erro geral: {$e->getMessage()}";
	        }
        }

        function selecionarCond($campo,$condicao){
	        try{
	            $conexao = new Conexao();	
	            $cmd = $conexao->getConexao()->prepare("SELECT * FROM site WHERE $campo=:cond;");
	            $cmd->bindParam("cond", $condicao);
	            $cmd->execute();
	            $resultado = $cmd->fetch(PDO::FETCH_OBJ);
	            return $resultado;
	            $conexao->fecharConexao();
	        }catch(PDOException $e){
	            echo "Erro no banco: {$e->getMessage()}";
	        }catch(Exception $e){
	            echo "Erro geral: {$e->getMessage()}";
	        }
     	}           	  

        function atualizar($id, $sitegeral){
            try{
            	$conexao = new Conexao();
    			$conteudo = $sitegeral->getConteudo();
                $cmd = $conexao->getConexao()->prepare("UPDATE site SET conteudo = :cont WHERE id=:id;");
                $cmd->bindParam("id", $id);
                $cmd->bindParam("cont", $conteudo);             
				if($cmd->execute()){
					$conexao->fecharConexao();
					return true;
				}else{
					$conexao->fecharConexao();
					return false;
				}
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
		
	}





?>