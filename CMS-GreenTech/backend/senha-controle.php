<?php  
	require_once("conexao.php");
	require_once("senha-modelo.php");

	class SenhaControle{	
			 function removerSenha($idadm){
	            try{
	                $conexao = new Conexao();
	                $cmd = $conexao->getConexao()->prepare("DELETE FROM senha WHERE senhaAdm=:idadm");
	                $cmd->bindParam("idadm", $idadm);
	                if($cmd->execute()){
	                	$conexao->fecharConexao();
	                    return true;
	                }else{
	                	$conexao->fecharConexao();
	                    return false;
	                }
	            }catch(PDOException $e){
	                echo "Erro de PDO: {$e->getMessage()}";
	                return false;
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	                return false;
	            }
	        }
			function selecionarTodosSenha(){
	            try{
	                $conexao = new Conexao();
	                $cmd = $conexao->getConexao()->prepare("SELECT * FROM senha;");
	                $cmd->execute();
	                $resultado = $cmd->fetchAll(PDO::FETCH_CLASS, "Senha");
	                return $resultado;
	                $conexao->fecharConexao();
	            }catch(PDOException $e){
	                echo "Erro no banco: {$e->getMessage()}";
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	            }
        	}

        	function selecionarUmaSenha($idadm){
	            try{
	                $conexao = new Conexao();	
	                $cmd = $conexao->getConexao()->prepare("SELECT * FROM senha WHERE senhaAdm=:idadm;");
	                $cmd->bindParam("idadm", $idadm);
	                $cmd->execute();
	                $resultado = $cmd->fetch(PDO::FETCH_OBJ);
	                return $resultado;
	                $conexao->fecharConexao();
	            }catch(PDOException $e){
	                echo "Erro no banco: {$e->getMessage()}";
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	            }
        	}
        	function selecionarCondSenha($campo,$condicao){
	            try{
	                $conexao = new Conexao();	
	                $cmd = $conexao->getConexao()->prepare("SELECT * FROM senha WHERE $campo=:cond;");
	                $cmd->bindParam("cond", $condicao);
	                $cmd->execute();
	                $resultado = $cmd->fetch(PDO::FETCH_OBJ);
	                return $resultado;
	                $conexao->fecharConexao();
	            }catch(PDOException $e){
	                echo "Erro no banco: {$e->getMessage()}";
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	            }
        	} 
    		function atualizarSenha($senha){
            	try{
                	$conexao = new Conexao();
					$senhaAtual = $senha->getSenhaAtual();
    				$senhaPassada = $senha->getSenhaPassada();
    				$senhaAdm = $senha->getSenhaAdm();
                	$cmd = $conexao->getConexao()->prepare("UPDATE senha SET senhaAtual = :sa, senhaPassada = :sp WHERE senhaAdm = :idadm;");
                	$cmd->bindParam("sa", $senhaAtual);
                	$cmd->bindParam("sp", $senhaPassada);
                	$cmd->bindParam("idadm", $senhaAdm);           
					if($cmd->execute()){
						$conexao->fecharConexao();
						return true;
					}else{
						$conexao->fecharConexao();
						return false;
					}
            	}catch(PDOException $e){
                	echo "Erro no banco: {$e->getMessage()}";
            	}catch(Exception $e){
                	echo "Erro geral: {$e->getMessage()}";
            	}
        	}
        	function inserirSenha($senha){
	            try{
	                $conexao = new Conexao();
					$senhaAtual = $senha->getSenhaAtual();
    				$senhaPassada = $senha->getSenhaPassada();
    				$senhaAdm = $senha->getSenhaAdm();
	                $cmd = $conexao->getConexao()->prepare("INSERT INTO senha(senhaAtual,senhaPassada,senhaAdm) VALUES(:sa,:sp,:sadm);");
	                $cmd->bindParam("sa", $senhaAtual);
                	$cmd->bindParam("sp", $senhaPassada);
                	$cmd->bindParam("sadm", $senhaAdm);
	                if($cmd->execute()){
	                    $conexao->fecharConexao();
	                    return true;
	                }else{
	                    $conexao->fecharConexao();
	                    return false;
	                }
	            }catch(PDOException $e){
	                echo "Erro do banco: {$e->getMessage()}";
	                return false;
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	                return false;
	            }
        	}
        	function confSenha($conf, $senha){
        		if ($conf==$senha) {
        			return true;
        		}else{
        			return false;
        		}
        	}
        	function criptografa($texto){
				$a=fopen("../imagens/listadeimagens.txt","r");
				$c=fread($a,4096);
				$cifragem="AES-128-CBC";
				$tamanho=openssl_cipher_iv_length($cifragem);
				$tm=openssl_random_pseudo_bytes($tamanho);
				$criptografar=openssl_encrypt($texto,$cifragem,$c,OPENSSL_RAW_DATA,$tm);
				$criptografar=base64_encode($tm.$criptografar);
				return $criptografar;
			}
			function descr($textc){
				$textc=base64_decode($textc);
				$a=fopen("../imagens/listadeimagens.txt","r");
				$chave=fread($a,4096);
				$cifragem="AES-128-CBC";
				$tamanho=openssl_cipher_iv_length($cifragem);
				$tm=mb_substr($textc,0,$tamanho,'8bit');
				$textc=mb_substr($textc,$tamanho,null,'8bit');
				$desc=openssl_decrypt($textc,$cifragem,$chave,OPENSSL_RAW_DATA,$tm);
				return $desc;	
			}



}



?>