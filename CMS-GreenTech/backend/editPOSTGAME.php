<?php
	require_once("midiapost-controle.php");
	require_once("post-controle.php");
try{
	$postcontrol = new PostControle();
	$post = new Post();
	$midiacontrol = new MidiapostControle();
	$midiapost = new Midiapost();
	$tabelaMidia = "midiagames";
	$tabela = "postgames";

	$arquivo = $_FILES['upload'];
	$id=$_GET['id'];
	$post->setTitulo($_POST['titulo']);
	$post->setLead($_POST['lead']);
	$post->setTexto($_POST['txtInserir']);
	if($arquivo['name']=="" && $arquivo['type']=="" && $arquivo['tmp_name']=="" ){
		if($postcontrol->atualizarPost($id, $post, $tabela)){
			echo "post texto editado com sucesso!";
			header("Location: ../adm/gerenciagame-adm.php");
		}else{
			echo "post texto n foi editado";
			header("Location: ../adm/editgame-adm.php");
		}		
	}else{
		$midiapost->setNome($arquivo['name']);
		$midiapost->setTipo($arquivo['type']);
		$midiapost->setConteudo(file_get_contents($arquivo['tmp_name']));
	
		if($midiapost->getTipo()=="image/png" || $midiapost->getTipo()=="image/jpg" || $midiapost->getTipo()=="image/jpeg" || $midiapost->getTipo()=="video/mp4" || $midiapost->getTipo()=="video/webm" || $midiapost->getTipo()=="image/webp" || $midiapost->getTipo()=="image/svg+xml" && $arquivo['size'] < 20000000 ){

			if($postcontrol->atualizarPost($id, $post, $tabela)){
				if($midiacontrol->atualizarMidia($id, $midiapost, $tabelaMidia)){
					echo "post editado com sucesso!";
					header("Location: ../adm/gerenciagame-adm.php");

				}else{
					echo "A imagem não pode ser editada";
					header("Location: ../adm/editgame-adm.php");
				}

			}else{
				echo "não foi possivel editar a parte de texto do post";
				header("Location: ../adm/editgame-adm.php");
			}

		}else{
			echo "arquivo muito pesado ou o arquivo n corresponde a uma imagem ou video";
			header("Location: ../adm/editgame-adm.php");
		}
	}	

}catch(Exception $e){
    echo "Erro: $e->getMessage()";
    header("Location: ../adm/editgame-adm.php");
}

?>