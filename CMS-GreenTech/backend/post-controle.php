<?php
	require_once("conexao.php");  
	require_once("post-modelo.php");
	class PostControle{
		function removerPost($id,$tabela){
	            try{
	                $conexao = new Conexao();
	                $cmd = $conexao->getConexao()->prepare("DELETE FROM $tabela WHERE id=:id");
	                $cmd->bindParam("id", $id);
	                if($cmd->execute()){
	                	$conexao->fecharConexao();
	                    return true;
	                }else{
	                	$conexao->fecharConexao();
	                    return false;
	                }
	            }catch(PDOException $e){
	                echo "Erro de PDO: {$e->getMessage()}";
	                return false;
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	                return false;
	            }
	        }
	        function selecionarTodos($tabela){
	            try{
	                $conexao = new Conexao();
	                $cmd = $conexao->getConexao()->prepare("SELECT * FROM $tabela;");
	                $cmd->execute();
	                $resultado = $cmd->fetchAll(PDO::FETCH_CLASS, "Post");
	                return $resultado;
	                $conexao->fecharConexao();
	            }catch(PDOException $e){
	                echo "Erro no banco: {$e->getMessage()}";
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	            }
        	}

        	function inserirPost($post, $tabela){
	            try{
	                $conexao = new Conexao();
	                $titulo = $post->getTitulo();
	                $lead = $post->getLead();
	                $texto = $post->getTexto();
	                $cmd = $conexao->getConexao()->prepare("INSERT INTO $tabela(titulo,lead,texto) VALUES(:t,:l,:txt);");
	                $cmd->bindParam("t", $titulo);
	                $cmd->bindParam("l", $lead);
	                $cmd->bindParam("txt", $texto);
	                if($cmd->execute()){
	                    $conexao->fecharConexao();
	                    return true;
	                }else{
	                    $conexao->fecharConexao();
	                    return false;
	                }
	            }catch(PDOException $e){
	                echo "Erro do banco: {$e->getMessage()}";
	                return false;
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	                return false;
	            }
        	}

			function selecionarUm($id, $tabela){
	            try{
	                $conexao = new Conexao();	
	                $cmd = $conexao->getConexao()->prepare("SELECT * FROM $tabela WHERE id=:id;");
	                $cmd->bindParam("id", $id);
	                $cmd->execute();
	                $resultado = $cmd->fetch(PDO::FETCH_OBJ);
	                return $resultado;
	                $conexao->fecharConexao();
	            }catch(PDOException $e){
	                echo "Erro no banco: {$e->getMessage()}";
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	            }
        	}

        	function selecionarCond($campo,$condicao, $tabela){
	            try{
	                $conexao = new Conexao();	
	                $cmd = $conexao->getConexao()->prepare("SELECT * FROM $tabela WHERE $campo=:cond;");
	                $cmd->bindParam("cond", $condicao);
	                $cmd->execute();
	                $resultado = $cmd->fetch(PDO::FETCH_OBJ);
	                return $resultado;
	                $conexao->fecharConexao();
	            }catch(PDOException $e){
	                echo "Erro no banco: {$e->getMessage()}";
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	            }
        	}             

        	function selecionarMultcond($post, $tabela){
	            try{
	                $conexao = new Conexao();
	                $titulo = $post->getTitulo();
	                $lead = $post->getLead();
	                $texto = $post->getTexto();
	                $cmd = $conexao->getConexao()->prepare("SELECT * FROM $tabela WHERE titulo=:cond1 AND lead=:cond2 AND texto=:cond3;");
	                $cmd->bindParam("cond1", $titulo);
	                $cmd->bindParam("cond2", $lead);
	                $cmd->bindParam("cond3", $texto);
	                $cmd->execute();
	                $resultado = $cmd->fetch(PDO::FETCH_OBJ);
	                return $resultado;
	                $conexao->fecharConexao();
	            }catch(PDOException $e){
	                echo "Erro no banco: {$e->getMessage()}";
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	            }
        	}    

        	function atualizarPost($id, $post, $tabela){
            	try{
                	$conexao = new Conexao();
					$titulo = $post->getTitulo();
	                $lead = $post->getLead();
	                $texto = $post->getTexto();
                	$cmd = $conexao->getConexao()->prepare("UPDATE $tabela SET titulo = :t, lead = :l, texto = :txt WHERE id=:id;");
                	$cmd->bindParam("id", $id);
                	$cmd->bindParam("t", $titulo);
	                $cmd->bindParam("l", $lead);
	                $cmd->bindParam("txt", $texto);             
					if($cmd->execute()){
						$conexao->fecharConexao();
						return true;
					}else{
						$conexao->fecharConexao();
						return false;
					}
            	}catch(PDOException $e){
                	echo "Erro no banco: {$e->getMessage()}";
            	}catch(Exception $e){
                	echo "Erro geral: {$e->getMessage()}";
            	}
        	}
		
	}

?>