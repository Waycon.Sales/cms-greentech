<?php
session_start();
require_once("midiasite-controle.php");
$midiacontrol = new MidiasiteControle();
$midiasite = new Midiasite();
$id=$_GET['id'];
$arquivo = $_FILES['midia'];
$midiasite->setNome($arquivo['name']);
$midiasite->setTipo($arquivo['type']);
$midiasite->setConteudo(file_get_contents($arquivo['tmp_name']));
if($midiasite->getTipo()=="image/png" || $midiasite->getTipo()=="image/jpg" || $midiasite->getTipo()=="image/jpeg" || $midiasite->getTipo()=="video/mp4" || $midiasite->getTipo()=="video/webm" || $midiasite->getTipo()=="image/webp" || $midiasite->getTipo()=="image/svg+xml" ){
	if ($arquivo['size'] < 20000000) {
		$midiacontrol->atualizar($id,$midiasite);
		header("Location: ../adm/home-adm.php");
	}else{
		echo "arquivo com mais de 2mb não são aceitos";
		header("Location: ../adm/modificarhome-adm.php");
	}

}else{
	echo "arquivo não é uma imagem ou video";
	header("Location: ../adm/modificarhome-adm.php");

}

?>