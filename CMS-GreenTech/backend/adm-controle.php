<?php  
	require_once("conexao.php");
	require_once("adm-modelo.php");

	class AdmControle{
	        function removerAdm($id){
	            try{
	                $conexao = new Conexao();
	                $cmd = $conexao->getConexao()->prepare("DELETE FROM adm WHERE id=:id");
	                $cmd->bindParam("id", $id);
	                if($cmd->execute()){
	                	$conexao->fecharConexao();
	                    return true;
	                }else{
	                	$conexao->fecharConexao();
	                    return false;
	                }
	            }catch(PDOException $e){
	                echo "Erro de PDO: {$e->getMessage()}";
	                return false;
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	                return false;
	            }
	        }
	        function selecionarTodosAdm(){
	            try{
	                $conexao = new Conexao();
	                $cmd = $conexao->getConexao()->prepare("SELECT * FROM adm;");
	                $cmd->execute();
	                $resultado = $cmd->fetchAll(PDO::FETCH_CLASS, "Adm");
	                return $resultado;
	                $conexao->fecharConexao();
	            }catch(PDOException $e){
	                echo "Erro no banco: {$e->getMessage()}";
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	            }
        	}

        	function inserirAdm($adm){
	            try{
	                $conexao = new Conexao();
	                $user = $adm->getUser();
	                $email = $adm->getEmail();
	                $cmd = $conexao->getConexao()->prepare("INSERT INTO adm(user,email) VALUES(:u,:e);");
	                $cmd->bindParam("u", $user);
	                $cmd->bindParam("e", $email);
	                if($cmd->execute()){
	                    $conexao->fecharConexao();
	                    return true;
	                }else{
	                    $conexao->fecharConexao();
	                    return false;
	                }
	            }catch(PDOException $e){
	                echo "Erro do banco: {$e->getMessage()}";
	                return false;
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	                return false;
	            }
        	}

			function selecionarUm($id){
	            try{
	                $conexao = new Conexao();	
	                $cmd = $conexao->getConexao()->prepare("SELECT * FROM adm WHERE id=:id;");
	                $cmd->bindParam("id", $id);
	                $cmd->execute();
	                $resultado = $cmd->fetch(PDO::FETCH_OBJ);
	                return $resultado;
	                $conexao->fecharConexao();
	            }catch(PDOException $e){
	                echo "Erro no banco: {$e->getMessage()}";
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	            }
        	}

        	function selecionarCond($campo,$condicao){
	            try{
	                $conexao = new Conexao();	
	                $cmd = $conexao->getConexao()->prepare("SELECT * FROM adm WHERE $campo=:cond;");
	                $cmd->bindParam("cond", $condicao);
	                $cmd->execute();
	                $resultado = $cmd->fetch(PDO::FETCH_OBJ);
	                return $resultado;
	                $conexao->fecharConexao();
	            }catch(PDOException $e){
	                echo "Erro no banco: {$e->getMessage()}";
	            }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	            }
        	}             

        	function atualizarAdm($id, $adm){
            	try{
                	$conexao = new Conexao();
					$user = $adm->getUser();
    				$email = $adm->getEmail();
                	$cmd = $conexao->getConexao()->prepare("UPDATE adm SET user = :user, email = :email WHERE id=:id;");
                	$cmd->bindParam("id", $id);
                	$cmd->bindParam("user", $user);
                	$cmd->bindParam("email", $email);             
					if($cmd->execute()){
						$conexao->fecharConexao();
						return true;
					}else{
						$conexao->fecharConexao();
						return false;
					}
            	}catch(PDOException $e){
                	echo "Erro no banco: {$e->getMessage()}";
            	}catch(Exception $e){
                	echo "Erro geral: {$e->getMessage()}";
            	}
        	}
        	

	}	
?>