<?php
	
	class Post {
		private $id;
		private $titulo;
		private $lead;
		private $texto;

		public function getId(){
            return $this->id;
        }
        public function getTitulo(){
            return $this->titulo;
        }
        public function getLead(){
            return $this->lead;
        }
        public function getTexto(){
            return $this->texto;
        }
        
        public function setId($i){
            $this->id = $i; 
        }
        public function setTitulo($t){
            $this->titulo = $t; 
        }
        public function setLead($l){
            $this->lead = $l; 
        }
        public function setTexto($txt){
            $this->texto = $txt; 
        }
	}

?>