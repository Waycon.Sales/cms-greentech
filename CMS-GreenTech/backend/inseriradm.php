<?php
require_once("adm-controle.php");
require_once("senha-controle.php");
try{
    $adm = new Adm();
    $senha = new Senha();
    $admcontrol = new AdmControle();
    $sencontrol = new SenhaControle();

    $adm->setUser($_POST['user']);
    $adm->setEmail($_POST['email']);

    $senha->setSenhaAtual($_POST['senha']);
    $senha->setSenhaPassada("");
    $confirmacao=$_POST['confsenha'];

    if($sencontrol->confsenha($confirmacao,$senha->getSenhaAtual())){
        $senha->setSenhaAtual($sencontrol->criptografa($senha->getSenhaAtual()));
        if($admcontrol->inserirAdm($adm)){
            $campo="user";
            $condicao=$_POST['user'];
            $dados=$admcontrol->selecionarCond($campo,$condicao);
            $id=$dados->id;
            $senha->setSenhaAdm($id);
            if($sencontrol->inserirSenha($senha)){
                $data = "Um novo administrador foi adicionado com sucesso!";
                echo $data;
            }else{
                $admcontrol->removerAdm($id);
                $data = "Não foi possivel adiciona um novo administrador!";
                echo $data;        
            }
        }
    }else{
        $data = "senha não é igual a confirmação!";
        echo $data;
    }
}catch(Exception $e){
    echo "Erro: $e->getMessage()";
    header("Location: ../adm/cadastro-adm.php");
}

?>