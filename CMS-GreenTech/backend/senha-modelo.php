<?php 	
class Senha{
		private $id;
		private $senhaAtual;
        private $senhaPassada;
        private $senhaAdm;
		public function getId(){
            return $this->id;
        }
        public function getSenhaAtual(){
            return $this->senhaAtual;
        }
        public function getSenhaPassada(){
            return $this->senhaPassada;
        }
        public function getSenhaAdm(){
            return $this->senhaAdm;
        }
        public function setId($i){
            $this->id = $i; 
        }
		public function setSenhaAtual($s){
            $this->senhaAtual = $s;
        }
        public function setSenhaPassada($sp){
            $this->senhaPassada = $sp;
        }
        public function setSenhaAdm($sa){
            $this->senhaAdm = $sa;
        }
    }
?>