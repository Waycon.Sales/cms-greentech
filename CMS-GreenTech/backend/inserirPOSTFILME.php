<?php  
	require_once("post-controle.php");
	require_once("midiapost-controle.php");
try{
	$tabela="postfilmes";
	$tabelaMidia="midiafilmes";
	$post = new Post();
	$postcontrole = new PostControle();
	$midiapost = new Midiapost();
	$midiacontrole = new MidiapostControle();

	$post->setTitulo($_POST['titulo']);
	$post->setLead($_POST['lead']);
	$post->setTexto($_POST['txtInserir']);

	$arquivo = $_FILES['upload'];
	$midiapost->setNome($arquivo['name']);
	$midiapost->setTipo($arquivo['type']);
	$midiapost->setConteudo(file_get_contents($arquivo['tmp_name']));

	if($midiapost->getTipo()=="image/png" || $midiapost->getTipo()=="image/jpg" || $midiapost->getTipo()=="image/jpeg" || $midiapost->getTipo()=="video/mp4" || $midiapost->getTipo()=="video/webm" || $midiapost->getTipo()=="image/webp" || $midiapost->getTipo()=="image/svg+xml" && $arquivo['size'] < 20000000 ){

		if($postcontrole->inserirPost($post, $tabela)){
			$dados=$postcontrole->selecionarMultcond($post,$tabela);
			$midiapost->setPost($dados->id);
			if($midiacontrole->inserirMidia($midiapost,$tabelaMidia)){
				echo "post adicionado com sucesso!";
				header("Location: ../adm/gerenciafilmes-adm.php");

			}else{
				$id = $dados->id; 
				$postcontrole->removerPost($id,$tabela);
				echo "a adição do post não foi efetuada";
				header("Location: ../adm/filmes-adm.php");
			}

		}else{
			echo "não foi possivel adcionar a parte de texto do post";
			header("Location: ../adm/filmes-adm.php");
		}

	}else{
		echo "arquivo muito pesado ou o arquivo n corresponde a uma imagem ou video";
		header("Location: ../adm/filmes-adm.php");
	}


}catch(Exception $e){
    echo "Erro: $e->getMessage()";
    header("Location: ../adm/filmes-adm.php");
}





?>