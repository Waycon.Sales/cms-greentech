<?php  
	require_once("conexao.php");
	require_once("midiasite-modelo.php");
	class MidiasiteControle{
		function selecionarTodos(){
	        try{
	            $conexao = new Conexao();
	            $cmd = $conexao->getConexao()->prepare("SELECT * FROM midiasite;");
	            $cmd->execute();
	            $resultado = $cmd->fetchAll(PDO::FETCH_CLASS, "Midiasite");
	            return $resultado;
	            $conexao->fecharConexao();
	        }catch(PDOException $e){
	                echo "Erro no banco: {$e->getMessage()}";
	        }catch(Exception $e){
	                echo "Erro geral: {$e->getMessage()}";
	        }
        }

        function selecionarUm($id){
	        try{
	           	$conexao = new Conexao();	
	           	$cmd = $conexao->getConexao()->prepare("SELECT * FROM midiasite WHERE id=:id;");
	            $cmd->bindParam("id", $id);
	            $cmd->execute();
	            $resultado = $cmd->fetch(PDO::FETCH_OBJ);
	         	return $resultado;
	            $conexao->fecharConexao();
	        }catch(PDOException $e){
	            echo "Erro no banco: {$e->getMessage()}";
	        }catch(Exception $e){
	            echo "Erro geral: {$e->getMessage()}";
	        }
        }

        function selecionarCond($campo,$condicao){
	        try{
	            $conexao = new Conexao();	
	            $cmd = $conexao->getConexao()->prepare("SELECT * FROM midiasite WHERE $campo=:cond;");
	            $cmd->bindParam("cond", $condicao);
	            $cmd->execute();
	            $resultado = $cmd->fetch(PDO::FETCH_OBJ);
	            return $resultado;
	            $conexao->fecharConexao();
	        }catch(PDOException $e){
	            echo "Erro no banco: {$e->getMessage()}";
	        }catch(Exception $e){
	            echo "Erro geral: {$e->getMessage()}";
	        }
     	}           	  

        function atualizar($id, $midiasite){
            try{
            	$conexao = new Conexao();
            	$nome = $midiasite->getNome();
            	$tipo = $midiasite->getTipo();
    			$conteudo = $midiasite->getConteudo();
                $cmd = $conexao->getConexao()->prepare("UPDATE midiasite SET nome = :nome, tipo = :tipo, conteudo = :cont WHERE id=:id;");
                $cmd->bindParam("id", $id);
                $cmd->bindParam("nome", $nome);
                $cmd->bindParam("tipo", $tipo);
                $cmd->bindParam("cont", $conteudo);             
				if($cmd->execute()){
					$conexao->fecharConexao();
					return true;
				}else{
					$conexao->fecharConexao();
					return false;
				}
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
		
	}





?>