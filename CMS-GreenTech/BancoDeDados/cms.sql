DROP DATABASE IF EXISTS blog; 
CREATE DATABASE IF NOT EXISTS blog;

CREATE TABLE blog.adm(
	id INT(3) NOT NULL AUTO_INCREMENT,
	user VARCHAR(30) NOT NULL,
	email VARCHAR(80) NOT NULL,
	PRIMARY KEY(id)
	);

CREATE TABLE blog.site(
	id INT(3) NOT NULL AUTO_INCREMENT,
	referente VARCHAR(20) NOT NULL,
	conteudo TEXT NOT NULL,
	PRIMARY KEY(id)
	);

CREATE TABLE blog.midiasite(
	id INT(3) NOT NULL AUTO_INCREMENT,
	referente VARCHAR(20) NOT NULL,
	nome VARCHAR(100) NOT NULL,
	tipo VARCHAR(50) NOT NULL,
	conteudo LONGBLOB NOT NULL,
	PRIMARY KEY(id)
	);

CREATE TABLE blog.postfilmes(
	id INT(3) NOT NULL AUTO_INCREMENT,
	titulo TEXT NOT NULL,
	lead TEXT NOT NULL,
	texto TEXT NOT NULL,
	PRIMARY KEY(id)
	);

CREATE TABLE blog.postgames(
	id INT(3) NOT NULL AUTO_INCREMENT,
	titulo TEXT NOT NULL,
	lead TEXT NOT NULL,
	texto TEXT NOT NULL,
	PRIMARY KEY(id)
	);

CREATE TABLE blog.posttec(
	id INT(3) NOT NULL AUTO_INCREMENT,
	titulo TEXT NOT NULL,
	lead TEXT NOT NULL,
	texto TEXT NOT NULL,
	PRIMARY KEY(id)
	);

CREATE TABLE blog.senha(
	id INT(3) NOT NULL AUTO_INCREMENT,
	senhaAtual VARCHAR(160) NOT NULL,
	senhaPassada VARCHAR(160) NULL,
	senhaAdm INT(3) NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(senhaAdm) REFERENCES adm(id)
 	);

CREATE TABLE blog.midiafilmes(
	id INT(3) NOT NULL AUTO_INCREMENT,
	nome VARCHAR(100) NOT NULL,
	tipo VARCHAR(50) NOT NULL,
	conteudo LONGBLOB NOT NULL,
	post INT(3) NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(post) REFERENCES postfilmes(id)
	);

CREATE TABLE blog.midiatec(
	id INT(3) NOT NULL AUTO_INCREMENT,
	nome VARCHAR(100) NOT NULL,
	tipo VARCHAR(50) NOT NULL,
	conteudo LONGBLOB NOT NULL,
	post INT(3) NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(post) REFERENCES posttec(id)
	);

CREATE TABLE blog.midiagames(
	id INT(3) NOT NULL AUTO_INCREMENT,
	nome VARCHAR(100) NOT NULL,
	tipo VARCHAR(50) NOT NULL,
	conteudo LONGBLOB NOT NULL,
	post INT(3) NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(post) REFERENCES postgames(id)
	);

INSERT INTO blog.adm(user,email) VALUES("adm","adm@gmail.com");
INSERT INTO blog.senha(senhaAtual,senhaAdm) VALUES("123456","1");

/*textos do site editaveis*/
INSERT INTO blog.site(referente,conteudo) VALUES("Descricao","Este blog tem como objetivo o entreterimento.");
INSERT INTO blog.site(referente,conteudo) VALUES("legenda1","Adicione legendas e fotos Editaveis de fundo.");
INSERT INTO blog.site(referente,conteudo) VALUES("legenda2","Adicione legendas e fotos Editaveis de fundo.");
INSERT INTO blog.site(referente,conteudo) VALUES("Facebook","#");
INSERT INTO blog.site(referente,conteudo) VALUES("Instagram","#");
INSERT INTO blog.site(referente,conteudo) VALUES("Sobre","Somos um grupo de blogueiros escritores, buscamos ao máximo alcancar o público Geek com assuntos que os interessa.");

/*midias do site editaveis*/
INSERT INTO blog.midiasite(referente,nome,tipo,conteudo) VALUES("slide","midia-IMAGEM-slide-01-Editavel",".png","IMAGEM-midia-editavel1");
INSERT INTO blog.midiasite(referente,nome,tipo,conteudo) VALUES("slide","midia-IMAGEM-slide-02-Editavel",".png","IMAGEM-midia-editavel2");
INSERT INTO blog.midiasite(referente,nome,tipo,conteudo) VALUES("slide","midia-IMAGEM-slide-03-Editavel",".png","IMAGEM-midia-editavel3");
INSERT INTO blog.midiasite(referente,nome,tipo,conteudo) VALUES("slide","midia-IMAGEM-slide-04-Editavel",".png","IMAGEM-midia-editavel4");
INSERT INTO blog.midiasite(referente,nome,tipo,conteudo) VALUES("slide","midia-IMAGEM-slide-05-Editavel",".png","IMAGEM-midia-editavel5");

INSERT INTO blog.midiasite(referente,nome,tipo,conteudo) VALUES("Video","midia-VIDEO-06-Editavel",".mp4","VIDEO-midia-editavel6");

INSERT INTO blog.midiasite(referente,nome,tipo,conteudo) VALUES("Imagem-Fundo1-HOME","midia-IMAGEM-BANNER-1-HOME-07-Editavel",".png","IMAGEM-midia-editavel7");
INSERT INTO blog.midiasite(referente,nome,tipo,conteudo) VALUES("Imagem-Fundo2-HOME","midia-IMAGEM-BANNER-2-HOME-07-Editavel",".png","IMAGEM-midia-editavel8");

INSERT INTO blog.midiasite(referente,nome,tipo,conteudo) VALUES("SHAPE/DEV","midia-SHAPE/DEV-09-Editavel",".png","IMAGEM-midia-editavel9");
INSERT INTO blog.midiasite(referente,nome,tipo,conteudo) VALUES("SHAPE/DEV","midia-SHAPE/DEV-10-Editavel",".png","IMAGEM-midia-editavel10");
INSERT INTO blog.midiasite(referente,nome,tipo,conteudo) VALUES("SHAPE/DEV","midia-SHAPE/DEV-11-Editavel",".png","IMAGEM-midia-editavel11");

INSERT INTO blog.midiasite(referente,nome,tipo,conteudo) VALUES("CAPA-FILMES","midia-CAPA-12-Editavel",".png","IMAGEM-midia-editavel12");
INSERT INTO blog.midiasite(referente,nome,tipo,conteudo) VALUES("CAPA-GAMES","midia-CAPA-13-Editavel",".png","IMAGEM-midia-editavel13");
INSERT INTO blog.midiasite(referente,nome,tipo,conteudo) VALUES("CAPA-TECNOLOGIA","midia-CAPA-14-Editavel",".png","IMAGEM-midia-editavel14");

INSERT INTO blog.midiasite(referente,nome,tipo,conteudo) VALUES("LOGO","midia-LOGO-15-Editavel",".png","IMAGEM-midia-editavel15");


INSERT INTO blog.postfilmes(titulo,lead,texto) VALUES("Edite esse post sobre Filmes ","Edite o post mas nunca o exclua, pois esse sempre aparecerá aqui na home","O Primeiro post de cada área do site sempre fica na home, então sempre edite mas não o exclua.");
INSERT INTO blog.postgames(titulo,lead,texto) VALUES("Edite esse post sobre Games","Edite o post mas nunca o exclua, pois esse sempre aparecerá aqui na home","O Primeiro post de cada área do site sempre fica na home, então sempre edite mas não o exclua.");
INSERT INTO blog.posttec(titulo,lead,texto) VALUES("Edite esse post sobre Tecnologia","Edite o post mas nunca o exclua, pois esse sempre aparecerá aqui na home","O Primeiro post de cada área do site sempre fica na home, então sempre edite mas não o exclua.");

INSERT INTO blog.midiafilmes(post,nome,tipo,conteudo) VALUES(1,"midia-POST-Filmes-1-Editavel",".png","IMAGEM-midiaFilmes-editavel1");
INSERT INTO blog.midiagames(post,nome,tipo,conteudo) VALUES(1,"midia-POST-Games-1-Editavel",".png","IMAGEM-midiaGames-editavel1");
INSERT INTO blog.midiatec(post,nome,tipo,conteudo) VALUES(1,"midia-POST-Tec-1-Editavel",".png","IMAGEM-midiaTec-editavel1");



