<?php  
require_once("backend/sitegeral-controle.php");
require_once("backend/post-controle.php");
$sitecontrol = new SitegeralControle();
$postcontrol = new PostControle();
$id=4;
$facelink = $sitecontrol->selecionarUm($id);
$id=5;
$instalink = $sitecontrol->selecionarUm($id);
$id=6;
$sobre = $sitecontrol->selecionarUm($id);
$tabela="postfilmes";
$dados=$postcontrol->selecionarTodos($tabela);
	echo '
		<html lang="pt-br">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/filmes.css">
    <link rel="stylesheet" type="text/css" href="css/pace.css">
    <link rel="shortcut icon" href="backend/carregasite-midia.php?id=15">
    <title>filmes-geek</title>
</head>
<body>
	<header>

		<div id="topo" class="container">
			
				<img class="logo img-fluid" src="backend/carregasite-midia.php?id=15" alt="logo">
			
			<div  class="nome-logo">
				<h1 class="nomesite"><strong>Geek Side</strong></h1>
				
			</div>
			<p class="lead">Melhor site Geek</p>
		</div>
	</header>
<!---------------------------------------------MENU---------------------------------------------------->

		<nav class="navbar navbar-expand-lg navbar-dark naveg">
   			<button class="navbar-toggler mr-auto" type="button" data-toggle="collapse" data-target="#menu">
   				<span class="navbar-toggler-icon"></span> 
   			</button>
			<div id="menu" class="collapse navbar-collapse">
				<ul class="navbar-nav m-auto">
					<li class="nave-item">
						<a class="link" href="index.php">Home</a>						
					</li>
					<li class="nave-item">
						<a class="link " href="zona-game.php">Zona-Game</a>						
					</li>
					<li class="nave-item">
						<a class=" link filmes" href="#">Filmes</a>						
					</li>
					<li class="nave-item">
						<a class="link" href="tecnologia.php">Tecnologia</a>						
					</li>
					<li class="nave-item">
						<a class="link" href="#footer">Sobre Nós</a>						
					</li>
				</ul>		
			</div>
		</nav>
<!------------------------------------conteudo---------------------------------->
	<main class="conteudo mt-5 mb-5">
		<div class="container">
		<div class="card-columns">
			';
				foreach($dados as $postfilme){
					
		 				echo"<div class='card mt-4 mb-4'>";
		    				echo "<img class='card-img-top' src='backend/carregafilme-midia.php?id="; echo $postfilme->getId(); echo "' alt='Imagem de capa do card'>";
		    				echo "<div class='card-body'>";
		      					echo"<h5 class='card-title'>"; 
		      						echo $postfilme->getTitulo(); 
		      					echo"</h5>";

		      					echo"<p class='card-text'>";
		      						 echo $postfilme->getLead();
		      					echo "</p>";
		    				echo "</div>";
		    				echo "<div class='card-footer'>";
		      					echo"<button class='btn'><a href='artigofilme.php?id="; echo $postfilme->getId(); echo "'>Ver mais</a> </button>";
		    				echo"</div>";
		  				echo"</div>";
		  		
		  		}
 			echo '	
		</div>
	</div>
	</main>

<!------------------------------------FOOTER----------------------------------->

			  <div class="container-fluid">
			    <div id="footer" class="footer">
			      <div class="footer-content">
			        <div class="row mx-lg-n5">
			        <div class="col-12 col-md voltar-topo">
			            <a href="#topo"><img src="imagens/topo.svg" alt="voltar ao topo"/></a>
			          </div>
			          <div class="col-6 col-md">
			            <h2 class=" txt-foot text-center">Sobre Nós</h2>
			            <div id="sobre" class="sobre">
			              '; echo $sobre->conteudo; echo'
			            </div>
			          </div>
			          <div class="col-6 col-md">
			            <h2 class="txt-foot text-center ">Siga-nos</h2>
			            <div class="text-center redes">
			              <a target="_blank" href="'; echo $facelink->conteudo; echo'"><span class="face">facebook</span></a>
			              <a target="_blank" href="'; echo $instalink->conteudo; echo'"><span class="insta">instagram</span></a>
			            </div>
			          </div>
			        </div>    
			      </div>
			      <div class="footer-bottom">
			        &copy; greentech | @waycon.sales
			      </div>
			    </div>
			  </div>
</div>
</div>

</body>
	<script src="js/jquery.min.js"></script>
  	<script src="js/bootstrap.min.js"></script>
  	<script src="js/pace.min.js"></script>
</html>
	


	';



?>