<?php
require_once("backend/sitegeral-controle.php");
require_once("backend/post-controle.php");
$sitecontrol = new SitegeralControle();
$postcontrol = new PostControle();
$id=1;
$tabela="postgames";
$postgame = $postcontrol->selecionarUm($id,$tabela);
$descricao = $sitecontrol->selecionarUm($id);
$tabela="postfilmes";
$postfilme = $postcontrol->selecionarUm($id,$tabela);
$tabela="posttec";
$posttec = $postcontrol->selecionarUm($id,$tabela);
$id=2;
$legenda1 = $sitecontrol->selecionarUm($id);
$id=3;
$legenda2 = $sitecontrol->selecionarUm($id);
$id=4;
$facelink = $sitecontrol->selecionarUm($id);
$id=5;
$instalink = $sitecontrol->selecionarUm($id);
$id=6;
$sobre = $sitecontrol->selecionarUm($id);




	


	echo'
			<html lang="pt-br">
			  <head>
			    <meta charset="utf-8">
			    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
			    <link rel="stylesheet" type="text/css" href="css/home.css">
			    <link rel="stylesheet" type="text/css" href="css/pace.css">
			    <link rel="shortcut icon" href="backend/carregasite-midia.php?id=15">
			    <title>Home</title>
			</head>
			<body>

<!----------------------------------------SLIDE---------------------------------------------->
			  <header id="topo">
			  	<div id="slideHome" class="carousel slide" data-ride="carousel">
			   	 <div class="carousel-inner">
			      	<div class="carousel-item active">
			        		<img id="img-1" class="d-block w-100 img img-fluid" src="backend/carregasite-midia.php?id=1" alt="Não foi possivel carregar o primeiro slide"/>
			      	</div>
			  	    <div class="carousel-item">
			  	     	<img id="img-2" class="d-block w-100 img" src="backend/carregasite-midia.php?id=2" alt="Não foi possivel carregar o segundo slide"/>

			  	    </div>
			  	    <div class="carousel-item">
			        	<img id="img-3" class="d-block w-100 img" src="backend/carregasite-midia.php?id=3" alt="Não foi possivel carregar o terceiro slide"/>
			      	</div>
			      	<div class="carousel-item">
			        		<img id="img-4" class="d-block w-100 img" src="backend/carregasite-midia.php?id=4" alt="Não foi possivel carregar o quarto slide"/>

			      	</div>
			       	<div class="carousel-item">
			        		<img id="img-5" class="d-block w-100 img" src="backend/carregasite-midia.php?id=5" alt="Não foi possivel carregar o quinto slide"/>
			      	</div>
			        </div>
			    	</div>
			  </header>

<!---------------------------------------------MENU------------------------------------------->

				<nav  class="navbar navbar-expand-lg navbar-dark naveg">
					<div class="logoEnome">
						<img class="logo mr-2" src="backend/carregasite-midia.php?id=15" alt="ainda sem logo">
			   			<span class="navbar-brand namesite"><strong>Geek Side</strong></span>
			   		</div>
			   		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu">
			   			<span class="navbar-toggler-icon"></span> 
			   		</button>
					<div id="menu" class="collapse navbar-collapse">
						<ul class="navbar-nav ml-auto">
							<li class="nave-item">
								<a class="link home " href="#">Home</a>						
							</li>
							<li class="nave-item">
								<a class=" link" href="zona-game.php">Zona Gamer</a>						
							</li>
							<li class="nave-item">
								<a class=" link" href="filmes.php">Filmes Geek</a>						
							</li>
							<li class="nave-item">
								<a class="link" href="tecnologia.php">Tecnologia</a>						
							</li>
			        <li class="nave-item">
			            <a class="link" href="#footer">Sobre Nós</a>           
			        </li>
							<li class="nave-item logtela">
								<a href="adm/login.php"><img class="login" src="imagens/iconuser.svg"></a>
							</li>
						</ul>		
					</div>
				</nav>

			   
<!-------------------------------APRESENTAÇÃO DO SITE --------------------------------------->

				<section class="text-apresenta">
					<div class="container text-center campo1">
						<div class="fundo">
							<h2 id="tituloHome" class="titulo">O que é o <strong>Geek Side</strong></h2>
						</div>
						<div class="desccontainer">
							<h6 id=" descricao" class="desctxt">
								<strong>
									'; echo $descricao->conteudo; echo'
								</strong>
							</h6>
						</div>
					</div>	
				</section>
				
<!------------------------------------------VIDEOHOME---------------------------------------->

				<section class="sessao-video">
			    	<div  class=" embed-responsive embed-responsive-16by9 videohome mb-5" >
			        	<video src="backend/carregasite-midia.php?id=6"  class="embed-responsive-item "  autoplay loop controls id="videoHome">
			        	</video>
			   		 </div>
				</section>     

<!---------------------------------IMAGENS DE FUNDO DIV----------------------------------->    
				<section class="p p1">      		
					<div class="container-fluid">			
						<div class="row mx-lg-n5 mt-5">
							<div class="back">
								<div class="txt txt1">
			  						<h2>
			  							<strong>
			  								'; echo $legenda1->conteudo; echo'
			  							</strong>
			  						</h2>
			  					</div>
			  				</div>
			  			</div>
					</div>
				</section>

				<section class="p p2">
					<div class="container-fluid">			
						<div class="row mx-lg-n5 mt-5 lin">
							<div class="back">
								<div class=" txt txt2">
			  						<h2>
			  							<strong>
			  								'; echo $legenda2->conteudo; echo'
			  							</strong>
			  						</h2>
			  					</div>
			  				</div>
			  			</div>
					</div>
				</section>
			<br><br><br><br><br><br>
<!--------------------------------CARDS ILUSTRATIVOS----------------------------------------->			
				<section class="sessao-cards">
					<div class="container">
						<div class="card-deck">
			 				<div class="card">
			    				<img class="card-img-top" src="backend/carregagame-midia.php?id=1" alt="Imagem de capa do card">
			    				<div class="card-body">
			      					<h5 class="card-title">'; echo $postgame->titulo; echo'</h5>
			      					<p class="card-text">
			      						'; echo $postgame->lead; echo'
			      					</p>
			    				</div>
			    				<div class="card-footer">
			      					<button class="btn"><a href="artigogame.php?id=1">Ver mais</a> </button>
			    				</div>
			  				</div>
			  				<div class="card">
			    				<img class="card-img-top" src="backend/carregafilme-midia.php?id=1" alt="Imagem de capa do card">
			    				<div class="card-body">
			      					<h5 class="card-title">'; echo $postfilme->titulo; echo'</h5>
			      					<p class="card-text">
			      						'; echo $postfilme->lead; echo'
			      					</p>
			    				</div>
			    				<div class="card-footer">
			      					<button class="btn"> <a href="artigofilme.php?id=1">Ver mais</a> </button>
			    				</div>
			  				</div>
			  				<div class="card">
			    				<img class="card-img-top" src="backend/carregatec-midia.php?id=1" alt="Imagem de capa do card">
			    				<div class="card-body">
			      					<h5 class="card-title">'; echo $posttec->titulo; echo'</h5>
			      					<p class="card-text">
			      						'; echo $posttec->lead; echo'
			      					</p>
			    				</div>
			    				<div class="card-footer">
			     					<button class="btn"> <a href="artigotec.php?id=1">Ver mais</a> </button>
			    				</div>
			  				</div>
						</div>
					</div>
				</section>
			<br><br><br>
<!---------------------------------DEV AND/OR SHAPES----------------------------------------->	

				<section class="circulos">			
					<div class="row mx-lg-n5 linha">
			  				<div><img class="dev img-fluid " src="backend/carregasite-midia.php?id=10" alt="erro-EDITE"/></div>
			  				<div ><img class=" img-fluid dev" src="backend/carregasite-midia.php?id=9" alt="erro-EDITE"/></div>
			    			<div ><img class="dev img-fluid" src="backend/carregasite-midia.php?id=11" alt="erro-EDITE"/></div>
					</div>
				</section >
			<br><br><br>
<!-------------------------------------FOOTER----------------------------------------------->

			  <div class="container-fluid">
			    <div id="footer" class="footer">
			      <div class="footer-content">
			        <div class="row mx-lg-n5">
			        <div class="col-12 col-md voltar-topo">
			            <a href="#topo"><img src="imagens/topo.svg" alt="voltar ao topo"/></a>
			          </div>
			          <div class="col-6 col-md">
			            <h2 class=" txt-foot text-center">Sobre Nós</h2>
			            <div id="sobre" class="sobre">
			              '; echo $sobre->conteudo; echo'
			            </div>
			          </div>
			          <div class="col-6 col-md">
			            <h2 class="txt-foot text-center">Siga-nos</h2>
			            <div class="text-center redes">
			              <a target="_blank" href="'; echo $facelink->conteudo; echo'"><span class="face">facebook</span></a>
			              <a target="_blank" href="'; echo $instalink->conteudo; echo'"><span class="insta">instagram</span></a>
			            </div>
			          </div>
			        </div>    
			      </div>
			      <div class="footer-bottom">
			        &copy; greentech | @waycon.sales
			      </div>
			    </div>
			  </div>


			</body>
			 	<script src="js/jquery.min.js"></script>
			  	<script src="js/bootstrap.min.js"></script>
				<script src="js/home.js"></script>
				<script src="js/pace.min.js"></script>
			</html>
';







?>
